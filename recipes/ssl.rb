
case node["deploy_env"]
 when nil, ''
 env='dev'
 when 'release'
 env='prod'
 when 'test'
 env='test'
 when 'prod'
 env='prod'
 when 'ci'
 env='dev'
 else
 env='dev'
end


### SSL setup for Apache
log node['hostname']
hname = node['hostname']

# Install mod_ssl package to enable ssl module in apache
package "mod_ssl" do
        action [:install]
end

# Create /etc/httpd/ssl directory on chef client
directory "#{node['apache']['dir']}/ssl" do
        action :create
        recursive true
        mode 0755
end

server_key = data_bag_item("projects", "demoi360")["server_key"]

file "#{node['apache']['dir']}/ssl/Apache_#{hname}.key" do
  content server_key
  group "#{node['apache']['group']}"
  user "#{node['apache']['user']}"
  mode '400'
  action :create_if_missing
end

server_cer = data_bag_item("projects", "demoi360")["server_cer"]

file "#{node['apache']['dir']}/ssl/Apache_#{hname}.cer" do
  content server_cer
  group "#{node['apache']['group']}"
  user "#{node['apache']['user']}"
  mode '400'
  action :create_if_missing
end

server_chaincert = data_bag_item("projects", "demoi360")["server_chaincert"]

file "#{node['apache']['dir']}/ssl/chaincert.crt" do
  content server_chaincert
  group "#{node['apache']['group']}"
  user "#{node['apache']['user']}"
  mode '400'
  action :create_if_missing
end

# This will make changes to ssl.conf
template "/etc/httpd/conf.d/ssl.conf" do
        source "ssl.conf.erb"
        mode 0644
        owner "root"
        group "root"
        variables(
                :sslcertificate => "#{node['apache']['sslpath']}/Apache_#{hname}.cer",
                :sslkey => "#{node['apache']['sslpath']}/Apache_#{hname}.key",
                :servername => "#{hname}.nibr.novartis.net",
                :sslchaincert  => "#{node['apache']['sslpath']}/chaincert.crt"
        )
end
## start httpd service

service "httpd" do
 action :restart
end




